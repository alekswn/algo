/* 
 * Compute MST using Prim's algorithm
 * Copyleft 2016  Alexey Novikov <alexey@novikov.io>
 */ 

#include <algorithm>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <deque>
#include <numeric>
#include <random>
#include <cassert>
#include <algorithm>
#include <iterator>
#include <cmath>
#include <limits>
#include <exception>
#include <list>
#include <set>
#include <thread>

#ifdef BENCH
#include <ctime>
#include <chrono>
    static std::chrono::high_resolution_clock::time_point t_start, t_end;
    std::clock_t c_start, c_end;
#define BENCHMARK_START \
    t_start = std::chrono::high_resolution_clock::now(); \
    c_start = std::clock();
#define BENCHMARK_FINISH( OUT_STREAM, NAME ) \
    c_end = std::clock(); \
    t_end = std::chrono::high_resolution_clock::now(); \
    OUT_STREAM   << "BENCHMARK \"" << NAME << "\" : " \
                 << std::fixed << "CPU time: " \
                 << 1000.0 * (c_end-c_start) / CLOCKS_PER_SEC << " ms; " \
                 << "Wall time: " \
                 << std::chrono::duration<double, std::milli>(t_end-t_start).count() \
                 << " ms\n";
#else
#define BENCHMARK_START 
#define BENCHMARK_FINISH( OUT_STREAM, NAME )
#endif


using namespace std; //Very, very bad, but it's damn convenient for a small program

typedef uint32_t index_t;
typedef int32_t weight_t;

static const weight_t INF_WEIGHT = numeric_limits<weight_t>::max(); 
static const index_t  FAKE_ROOT  = 0;

class Graph
{
protected:
    struct Edge {
        weight_t w;
        index_t u,v;
        Edge() : w(INF_WEIGHT), u(FAKE_ROOT), v(FAKE_ROOT) {};
        Edge(index_t u, index_t v, weight_t w) :w(w), u(u), v(v){};
        bool operator< (const Edge& r) const { return w < r.w; };
    };
    typedef vector< Edge > Edges;
private:
    size_t N;
    Edges edges;

public:
    static Graph readGraph( istream& s ); 
    string dump() const;
    size_t size() const {return N;}
    
    weight_t MST_Prim() const;
    
protected:
    Graph(size_t N) : N(N) {};
};

Graph Graph::readGraph( istream& s ) 
{
    index_t N,M;
    if ( !(s >> N >> M) ) 
        throw runtime_error("Bad input: graph dimentions expected");
    Graph G(N);
    index_t u,v;
    weight_t w;
    size_t m = 0;
    while (s >> u >> v >> w) {
        if (u > N || v > N) runtime_error("Bad input: invalid vertex");
        G.edges.emplace_back(u,v,w);
        m++;
    }
    if (M != m) throw runtime_error("Bad input: wrong number of edges");
    return std::move(G);
}

string Graph::dump() const
{
    ostringstream os;
    //matrix representation of a graph
    vector< vector< weight_t > > matrix( N+1, vector< weight_t >(N+1, INF_WEIGHT));

        for( const Edge& e : edges )
            matrix[e.u][e.v] = e.w;

    os.width(8); os << '+';
    for (index_t i = 1; i <= N; i++ ) {os.width(8); os << i;}
    os << endl;
    for (index_t i = 1; i <= N; i++) {
        const vector< weight_t >& line = matrix[i];
        os.width(8); os << i;
        for (index_t j = 1; j <= N; j++) {
            weight_t w = line[j];
            os.width(8); 
            (w != INF_WEIGHT) ? os << w : os << "@";
        }
        os << endl;
    }
    return os.str();
}

weight_t Graph::MST_Prim() const {
    vector< bool > X(N + 1, false);
    weight_t lng = 0;
    size_t cnt  = 1;
    X[1] = true;
    while ( cnt < N ) {
        weight_t minW = INF_WEIGHT;
        size_t minI;
        for ( size_t i = 0; i < edges.size(); i++ ) {
            const Edge& e = edges[i];
            if (minW > e.w && ( X[e.u] ^ X[e.v] )) {
                minW = e.w;
                minI = i;
            }
        }
        const Edge& e = edges[minI];
        X[e.u] = X[e.v] = true;
        lng += e.w;
        cnt++;
    }
    return lng;
}

int main(int argc, char* argv[])
{
    try {

        BENCHMARK_START
        Graph graph = Graph::readGraph(cin);
        BENCHMARK_FINISH( cerr, "readGraph" )       
        #ifdef DEBUG
            cerr <<  graph.dump() << endl;
        #endif
        BENCHMARK_START
        weight_t cost = graph.MST_Prim();
        BENCHMARK_FINISH( cerr, "Prim's MST" )       
        cout << cost << endl;
        return 0;
    } catch(exception& e) {
        cerr << "Error : " << e.what() << endl;
        return 1;
    }
}
