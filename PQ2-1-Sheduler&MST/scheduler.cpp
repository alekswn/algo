/* 
 * 
 * Copyleft 2016  Alexey Novikov <alexey@novikov.io>
 */ 

#include <algorithm>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <deque>
#include <numeric>
#include <random>
#include <cassert>
#include <algorithm>
#include <iterator>
#include <cmath>
#include <limits>
#include <exception>
#include <list>
#include <queue>
#include <forward_list>
#include <set>
#include <thread>


#ifdef BENCH
#include <ctime>
#include <chrono>
    static std::chrono::high_resolution_clock::time_point t_start, t_end;
    std::clock_t c_start, c_end;
#define BENCHMARK_START \
    t_start = std::chrono::high_resolution_clock::now(); \
    c_start = std::clock();
#define BENCHMARK_FINISH( OUT_STREAM, NAME ) \
    c_end = std::clock(); \
    t_end = std::chrono::high_resolution_clock::now(); \
    OUT_STREAM   << "BENCHMARK \"" << NAME << "\" : " \
                 << std::fixed << "CPU time: " \
                 << 1000.0 * (c_end-c_start) / CLOCKS_PER_SEC << " ms; " \
                 << "Wall time: " \
                 << std::chrono::duration<double, std::milli>(t_end-t_start).count() \
                 << " ms\n";
#else
#define BENCHMARK_START 
#define BENCHMARK_FINISH( OUT_STREAM, NAME )
#endif


using namespace std; //Very, very bad, but it's damn convenient for a small program


typedef pair<int, int> Job; //weight, length


int main() {
    auto compareDiff = [](Job a, Job b) { 
        int d = (a.first - a.second) - (b.first - b.second);
        if (d < 0) return true;
        if (d > 0) return false;
        return a.first < b.first; 
    };
    priority_queue<Job, vector<Job>, decltype(compareDiff)> jobsDiff(compareDiff);
    auto compareRatio = [](Job a, Job b) { return (a.first*b.second) < (b.first*a.second); };
    priority_queue<Job, vector<Job>, decltype(compareRatio)> jobsRatio(compareRatio);

    //Read input
    int n;
    cin >> n;
    uint w, l;
    while (cin >> w >> l) {
        jobsDiff.push(make_pair(w,l));
        jobsRatio.push(make_pair(w,l));
    }
    
    //Calculate weighted finishing time
    uint time = 0;
    uint64_t sum = 0;
    while (!jobsDiff.empty()) {
        const Job &job = jobsDiff.top();
        time += job.second;
        sum += job.first*time;
        #ifdef DEBUG
        cerr << job.first << " " << job.second << " " << (job.first - job.second) << " " << time << " " << sum << endl;
        #endif
        jobsDiff.pop();
    }
    cout << "1: " << sum << endl;
    
    sum = time = 0; 
    while (!jobsRatio.empty()) {
        const Job &job = jobsRatio.top();
        time += job.second;
        sum += job.first*time;
        #ifdef DEBUG
        cerr << job.first << " " << job.second << " " << (float(job.first)/job.second) << " " << time << " " << sum << endl;
        #endif
        jobsRatio.pop();
    }
    cout << "2: " << sum << endl;
    
    return 0;

}
