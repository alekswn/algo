/* 
 * Count the number of inversions in the accending sequence of positive numbers. 
 * Copyleft 2015  Alexey Novikov <alexey@novikov.io>
 * Inspaired by http://www.geeksforgeeks.org/merge-sort-for-linked-list/
 */ 

#include <iostream>
#include <forward_list>
#include <string>
#include <sstream>
#include <vector>
using namespace std;

typedef uint32_t data_t;
typedef forward_list<data_t> List;

inline bool isInversed(data_t first, data_t second)
{
    return (first < second);
}

string dump(List& list)
{
    ostringstream os;
    cout << endl;
    int i = 0;
    for (auto it = list.begin(); it != list.end(); it++) os << i++ << " : " << *it << endl;
    return os.str();
}


uint merge_count(List& head, List& tail)
{
    uint res = 0;
    uint factor = 0;
    if (tail.empty()) return 0;
    if (head.empty()) 
    {
        head = move(tail);
        return 0;
    }

    if (isInversed(head.front(), tail.front())) 
    {
        factor++;
        head.push_front(tail.front());
        tail.pop_front();
    }
    auto hit = head.begin();
    auto pit = hit;
    while (++hit != head.end())
    {
        if (!tail.empty() && isInversed(*hit, tail.front()))
        {
            factor++;
            hit = head.insert_after(pit, tail.front());
            tail.pop_front();
        } else {
            res += factor;
        }
        pit = hit;
    }
    if (!tail.empty()) head.splice_after(pit, tail);
    return res;
}

List split(List& head)
{
    if(head.empty()) return List();

    auto fast = head.begin();
    auto slow = fast;
    if ((++fast) == head.end()) return List(); //One entry only
    
    List tail;
//  Advance 'fast' two nodes, and advance 'slow' one node
    while (fast != head.end() && ++fast != head.end())
    {
        ++slow;
        ++fast;
    }

//  'slow' is before the midpoint in the list, so split it in two at that point
    auto midpoint = slow;
    tail.push_front(*(++slow));
    tail.splice_after(tail.begin(), head, slow, fast);
    head.erase_after(midpoint);
    return tail;
}

uint sort_count(List& head)
{
#ifdef DEBUG
    cerr << __PRETTY_FUNCTION__ << dump(head) <<  " |-->" << endl;
#endif
    uint res = 0;
    if (head.empty()) return 0;
    
    List tail = move(split(head));
    if (tail.empty()) return 0; //there was 1 entry only

    res += sort_count(head);
    res += sort_count(tail);
    res += merge_count(head, tail);
#ifdef DEBUG
    cerr << "<--| " << __PRETTY_FUNCTION__ << dump(head) << " : " << res << endl;
#endif
    return res;
}

#ifdef BRUTE_FORCE
uint brute_force(const List& head)
{
    uint res = 0;
    for (auto it = head.begin(); it != head.end(); it++)
        for (auto jt = it; jt != head.end(); jt++)
            if (isInversed(*it, *jt)) res++;
    return res;
}
#endif

int main()
{
    //Treat ',' as whitespace for cin
    const auto temp = ctype<char>::classic_table();
    vector<ctype<char>::mask> bar(temp, temp + ctype<char>::table_size);
    bar[','] |= ctype_base::space;
    cin.imbue(locale(cin.getloc(), new ctype<char>(bar.data())));
    
    List list; 
    while (!cin.eof()) 
    {
        data_t i;
        if (cin >> i) list.push_front(i);
    }
    cout << endl << dump(list) << endl;
#ifdef BRUTE_FORCE
    cout << brute_force(list) << endl;
#endif
    cout << sort_count(list) << endl;
    return 0;
}
        
    

