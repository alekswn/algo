/* 
 * Traveling salesman problem
 * Copyleft 2016  Alexey Novikov <alexey@novikov.io>
 */ 

#include <algorithm>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <queue>
#include <numeric>
#include <random>
#include <cassert>
#include <algorithm>
#include <iterator>
#include <cmath>
#include <limits>
#include <exception>
#include <list>
#include <thread>

#ifdef BENCH
#include <ctime>
#include <chrono>
    static std::chrono::high_resolution_clock::time_point t_start, t_end;
    std::clock_t c_start, c_end;
#define BENCHMARK_START \
    t_start = std::chrono::high_resolution_clock::now(); \
    c_start = std::clock();
#define BENCHMARK_FINISH( OUT_STREAM, NAME ) \
    c_end = std::clock(); \
    t_end = std::chrono::high_resolution_clock::now(); \
    OUT_STREAM   << "BENCHMARK \"" << NAME << "\" : " \
                 << std::fixed << "CPU time: " \
                 << 1000.0 * (c_end-c_start) / CLOCKS_PER_SEC << " ms; " \
                 << "Wall time: " \
                 << std::chrono::duration<double, std::milli>(t_end-t_start).count() \
                 << " ms\n";
#else
#define BENCHMARK_START 
#define BENCHMARK_FINISH( OUT_STREAM, NAME )
#endif


using namespace std; //Very, very bad, but it's damn convenient for a small program

const int MAX_TSP_PROBLEM_SIZE = 16;

typedef float dist_t;
typedef float coord_t;
typedef pair<coord_t, coord_t> Point;
typedef vector<Point> Points;

static const dist_t INF_DIST = numeric_limits<dist_t>::infinity(); 

class Tsp
{
    const size_t N;
    vector<dist_t> C;
public:
    Tsp ( const Points& );
    dist_t solve() const;
protected:
    dist_t dist(size_t p1, size_t p2) const;
};

Tsp::Tsp( const Points& P ) : N(P.size())
{
    C.reserve(N*(N+1)/2);
    for( size_t i = 0; i < N; i++ ) {
        for (size_t j = i+1; j < N; j++) {
            const size_t k = j*(j-1)/2 + i;
            C[k] = hypot(P[i].first - P[j].first, P[i].second - P[j].second);
            #ifdef DEBUG
            cerr << "\t(" << i << "," << j << ") -> C[" << k << "] : " << C[k] << endl;
            #endif 
        }
    }
}

dist_t Tsp::solve() const{
    #ifdef DEBUG
    cerr << "Solving problem of size " << N << endl;
    #endif
    size_t p2N = pow(2, N);
    dist_t res = INF_DIST;
    vector<vector<dist_t>> A( p2N + 1, vector<dist_t>(N + 1, INF_DIST) );     
    A[1][1] = 0;
    for (size_t m = 2; m <= N; m++) {
        size_t p2m = pow(2, m) - 1;
        for (size_t S = 3; S <= p2m; S++) {
            if (S%2 == 0) continue;
            for (size_t j = 2; j <= m; j++) {
                for (size_t k = 1; k <= m; k++) {
                    if (k==j) continue;
                    size_t S_j = S&~(1<<(j-1));
                    A[S][j] = min(A[S][j], A[S_j][k] + dist(k,j));
                    //cerr << m << " " << S << " " << j << " " << k << " : " << int(S_j)  << " " <<  A[S_j][k] << " " << A[S][j] << endl;
                }
            }
        }
        res = INF_DIST;
        for ( size_t j = 2; j <= m; j++ ) {
            res = min(res, A[p2m][j] + dist(j, 1));
        }

        #ifdef DEBUG
        cerr  << m << " "<< p2m << " : " << res << endl;
        #endif
    }

    return res;

}

dist_t Tsp::dist(size_t p1, size_t p2) const {
    if(p1 == p2) return 0;
    const size_t i = min(--p1, --p2);
    const size_t j = p1^p2^i;
    const size_t k = j*(j-1)/2 + i;
    
    return C[k];
}

Points readPoints() 
{
    Points P;
    size_t N;
    cin >> N;
    P.reserve(N);
    coord_t x,y;
    while (cin >> x >> y) {
        P.emplace_back(make_pair(x,y));
    }
    if (P.size() != N) throw runtime_error("Bad input file");
    return P;
}

int main(int argc, char* argv[])
{
    Points P = readPoints();
        priority_queue<size_t, vector<size_t>, greater<size_t>> fixedPoints;
        fixedPoints.push(P.size() - 1);
        while (--argc > 0) fixedPoints.push(atoi(*(++argv)));
        
        list<Tsp> problems;
        size_t lastPoint = 0;
        BENCHMARK_START
        while(!fixedPoints.empty()) {
            #ifdef DEBUG
            cerr << "Fixed point "<< fixedPoints.top() << endl;
            #endif
            problems.emplace_back(Points(next(begin(P), lastPoint), next(begin(P), 
                                                            fixedPoints.top()+1)));
            lastPoint = fixedPoints.top();
            fixedPoints.pop();
        }
        BENCHMARK_FINISH( cerr, "Tsp constructor" )
        dist_t res = 0;
        BENCHMARK_START
        for ( const Tsp& p : problems ) {
            //threads.emplace_back(&Tsp::solve, p, ref(results.back()));
            res+=p.solve();
        }
        BENCHMARK_FINISH( cerr, "Tsp solver" )
        cout << res << endl;
        return 0;

}
