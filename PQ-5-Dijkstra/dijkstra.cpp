/* 
 * Find shortest pathst on a unordered weighted graph 
 * Copyleft 2015  Alexey Novikov <alexey@novikov.io>
 */ 

#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <deque>
#include <numeric>
#include <random>
#include <cassert>
#include <algorithm>
#include <iterator>
#include <set>
#include <cmath>
#include <unordered_map>

#ifdef BENCH
#include <ctime>
#include <chrono>
    static std::chrono::high_resolution_clock::time_point t_start, t_end;
    std::clock_t c_start, c_end;
#define BENCHMARK_START \
    t_start = std::chrono::high_resolution_clock::now(); \
    c_start = std::clock();
#define BENCHMARK_FINISH( OUT_STREAM, NAME ) \
    c_end = std::clock(); \
    t_end = std::chrono::high_resolution_clock::now(); \
    OUT_STREAM   << "BENCHMARK \"" << NAME << "\" : " \
                 << std::fixed << "CPU time: " \
                 << 1000.0 * (c_end-c_start) / CLOCKS_PER_SEC << " ms; " \
                 << "Wall time: " \
                 << std::chrono::duration<double, std::milli>(t_end-t_start).count() \
                 << " ms\n";
#else
#define BENCHMARK_START 
#define BENCHMARK_FINISH( OUT_STREAM, NAME )
#endif


using namespace std; //Very, very bad, but it's damn convenient for a small program

typedef uint8_t index_t;
typedef uint32_t weight_t;

class Graph
{
public:
    typedef pair< index_t, weight_t > Edge;
    typedef set< Edge > Edges;
    typedef Edges Vertex;
    typedef vector< Vertex > Vertexes;
    const weight_t MAX_DST = (static_cast< weight_t> (-1)) >> 1; 
private:
    Vertexes vertexes;

public:
    Graph() noexcept : vertexes() {}
    Graph( Vertexes&& vertexes ) : vertexes(std::move(vertexes)) {}

    size_t size() const noexcept { return vertexes.size(); };
    string dump() const;

    vector< weight_t > dijkstra_naive( index_t s ) const;
    vector< weight_t > dijkstra_better ( index_t s ) const;
    vector< weight_t > dijkstra_heap ( index_t s ) const;

protected:
};

Graph readGraph( istream& s ) 
{
    string istr;
    Graph::Vertexes vertexes;
    while (getline( cin, istr ))
    {
        istringstream iss (istr);
        uint v,u,w, max_u;
        char ch;
        if (iss >> v) { 
            if ( v >= vertexes.size() ) vertexes.resize(v+1);
        } else throw domain_error ("Bad input: every line must begin with vertex number");
        while (iss >> u && iss >> ch && ch ==',' &&  iss >> w) {
            if ( u >= vertexes.size() ) vertexes.resize(u+1);
            vertexes[v].emplace( u,w );
            vertexes[u].emplace( v,w );
        }
    }
    Graph graph( std::move(vertexes) );
    return graph; 
}

string Graph::dump() const
{
    uint i;
    ostringstream os;
    //matrix representation of a graph
    vector< vector< weight_t > > matrix( size(), vector< weight_t >(size(), 0));

    for( int i = 0; i < size(); i++ )
        for( Edge edge : vertexes[i] )
            matrix[i][edge.first] = edge.second;

    os.width(8); os << '+';
    for (i = 0; i < size(); i++) {os.width(8); os << i;}
    os << endl;
    i = 0;
    for (vector< weight_t > line : matrix) {
        os.width(8); os << i++;
        for (weight_t w: line) {os.width(8); os << w;}
        os << endl;
    }
    return os.str();
}

vector< weight_t > Graph::dijkstra_naive( index_t s ) const
{
    vector < weight_t > A(size(), MAX_DST);
    vector < bool > X(size(), false);

    A[s] = 0;
    weight_t min_dst;
  
    do {
        X[s] = true;
        min_dst = MAX_DST;
        for( index_t v = 0; v < size(); v++ ) {
            if (X[v] == true) continue;
            for ( Edge edge : vertexes[v] ) {
                if (X[edge.first] == false) continue;
                weight_t dst = A[edge.first] + edge.second;
                if (dst < A[v]) A[v] = dst;
                if (A[v] < min_dst) {
                    min_dst = A[v];
                    s = v;
                }
            #ifdef DEBUG
                cerr << "\t" << __PRETTY_FUNCTION__ << " : " << (int)v << " " << (int)edge.first << " " << dst << " | " << (int)s << " " << min_dst  << endl;
            #endif
            }
        }
        #ifdef DEBUG
            cerr << __PRETTY_FUNCTION__ << " : " << (int)s << " " << min_dst << endl;
        #endif
    } while (min_dst < MAX_DST); //Remaining vertexes are not connected
    return A;
}

vector< weight_t > Graph::dijkstra_better( index_t s ) const
{
    vector < weight_t > A(size(), MAX_DST);
    vector < bool > X(size(), false);
    typedef pair < index_t, Edge > Candidat;
    typedef unordered_multimap < index_t, Edge > Candidates;
    Candidates candidates;

    A[s] = 0;
    weight_t min_dst;
    for (Edge edge: vertexes[s])
        candidates.emplace( edge.first, make_pair( s, edge.second ) );
  
    while (!candidates.empty()) {
        X[s] = true;
        min_dst = MAX_DST;
        for( Candidat c : candidates ) {
            weight_t dst = A[c.second.first] + c.second.second;
            A[c.first] = min(A[c.first], dst);
            if (A[c.first] < min_dst) min_dst = A[s = c.first];
            #ifdef DEBUG
                cerr << "\t" << __PRETTY_FUNCTION__ << " : " << (int)c.first << " " << (int)c.second.first << " " << dst << " | " << (int)s << " " << min_dst  << endl;
            #endif
        }
        candidates.erase(s);
        for (Edge edge: vertexes[s])
            if (X[edge.first] == false)
                candidates.emplace( edge.first, make_pair( s, edge.second ) );
        #ifdef DEBUG
            cerr << __PRETTY_FUNCTION__ << " : " << (int)s << " " << min_dst << endl;
        #endif
    }
    return A;
}

vector< weight_t > Graph::dijkstra_heap( index_t s ) const
{
    vector < weight_t > A(size(), MAX_DST);
    vector < bool > X(size(), false);
    
    class VX {
        vector < pair< weight_t, index_t > > heap;
        vector < index_t > indx;
        size_t heap_size;
    public:
        VX( size_t size, weight_t def_val ) 
            : heap_size(0)
        {
            heap.reserve(size);
            indx.reserve(size);
            for ( int i = 0; i < size; i++ ) {
                heap.push_back(make_pair( def_val, i ) );
                indx.push_back(i);
            }
        }
        void set( index_t i, weight_t v ) {
            heap[indx[i]].first = v;
            if (indx[i] >= heap_size) swap(indx[i], heap_size++);
            else down(indx[i]);
            up(indx[i]);
        }
        void pop() {
            swap(0, --heap_size);
            down(0);
        }
        weight_t get( index_t i ) const { return heap[indx[i]].first; }
        index_t  top() const { return heap.front().second; }
        weight_t  top_val() const { return heap.front().first; }
    private:
        void swap ( index_t i, index_t j ) {
            std::swap(heap[i], heap[i]);
            std::swap(indx[heap[i].second], indx[heap[j].second]);
        }
        void up (index_t i) {
            index_t p = parent(i);
            if (heap[p] <= heap[i]) return;
            swap( p, i );
            up(p);
        }
        void down (index_t i) {
            index_t c = succ(i);
            if (heap[c] >= heap[i]) return;
            swap( c, i );
            down(c);
        }
        index_t parent( index_t i ) { return ((i+1)>>1)-1; }
        index_t succ( index_t i ) {
            i++;
            index_t l = i<<1;
            index_t r = l+1;
            return (heap[l] <= heap[r]) ? (l) : (r);
        }
    } vx(size(), MAX_DST);     
    vx.set(s,0);
    weight_t min_dst;
    do {
        s = vx.top();
        A[s] = vx.top_val();
        vx.pop();
        for ( Edge edge : vertexes[s] ) {
             weight_t dst = A[s] + edge.second;
             if (dst < vx.get( edge.first )) {
                 vx.set(edge.first, dst);
                #ifdef DEBUG
                    cerr << "\t" << __PRETTY_FUNCTION__ << " : " << (int)s << " " << (int)edge.first << " " << dst  << endl;
                #endif
             }
        }
        #ifdef DEBUG
            cerr << __PRETTY_FUNCTION__ << " : " << (int)vx.top() << " " << vx.top_val()  << endl;
        #endif
    } while (vx.top_val() != MAX_DST); //Remaining vertexes are not connected
    return A;
}

int main(int argc, char* argv[])
{
    if ( argc < 2 ) {
        cerr << "Too few arguments" << endl;
        return 1;
    }
    index_t s = stoi(*(++argv));
    deque< index_t > tt;
    for( argc-=2; argc > 0; argc-- ) tt.push_back( stoi(*(++argv)) );
    try {
        BENCHMARK_START
            Graph graph = readGraph(cin);
        BENCHMARK_FINISH( cerr, "readGraph" )       
        cerr << "Graph size : "<< graph.size() << endl;
        #ifdef DEBUG
            cerr <<  graph.dump() << endl;
        #endif
        if ( s >= graph.size() ) throw range_error("Invalid source vertex");
        BENCHMARK_START
            vector< weight_t > A = graph.dijkstra_naive(s);
        BENCHMARK_FINISH( cerr, "Dijkstra naive" )
        #ifdef DEBUG
            cerr << "Dijkstra naive:" << endl; 
            for ( int i = 0; i < A.size(); i++ )
                cerr << "\t" << i << "\t" << A[i] << endl;
        #endif
        for (index_t t : tt) {
            if ( t >= graph.size() ) throw range_error("Invalid dest vertex");
            cout << A[t] << ",";
        }
        cout << '\b';
        cout << endl;
        BENCHMARK_START
            vector< weight_t > Ab = graph.dijkstra_better(s);
        BENCHMARK_FINISH( cerr, "Dijkstra better" )
        #ifdef DEBUG
            cerr << "Dijkstra better:" << endl; 
            for ( int i = 0; i < Ab.size(); i++ )
                cerr << "\t" << i << "\t" << Ab[i] << endl;
        #endif
        for (index_t t : tt) {
            cout << Ab[t] << ",";
        }
        cout << '\b';
        cout << endl;
        BENCHMARK_START
            vector< weight_t > Ah = graph.dijkstra_better(s);
        BENCHMARK_FINISH( cerr, "Dijkstra heap" )
        #ifdef DEBUG
            cerr << "Dijkstra heap:" << endl; 
            for ( int i = 0; i < Ah.size(); i++ )
                cerr << "\t" << i << "\t" << Ah[i] << endl;
        #endif
        for (index_t t : tt) {
            cout << Ah[t] << ",";
        }
        cout << '\b';
        cout << endl;
        return 0;
    } catch(exception& e) {
        cerr << "Error : " << e.what() << endl;
        return 1;
    }
}

