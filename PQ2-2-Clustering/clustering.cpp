/* 
 * Find shortest pathst on a unordered weighted graph 
 * Copyleft 2015  Alexey Novikov <alexey@novikov.io>
 */ 

#include <iostream>
#include <iomanip>
#include <string>
#include <sstream>
#include <vector>
#include <deque>
#include <numeric>
#include <cassert>
#include <algorithm>
#include <iterator>
#include <cmath>
#include <set>
#include <queue>
#include <map>

#ifdef BENCH
#include <ctime>
#include <chrono>
    static std::chrono::high_resolution_clock::time_point t_start, t_end;
    std::clock_t c_start, c_end;
#define BENCHMARK_START \
    t_start = std::chrono::high_resolution_clock::now(); \
    c_start = std::clock();
#define BENCHMARK_FINISH( OUT_STREAM, NAME ) \
    c_end = std::clock(); \
    t_end = std::chrono::high_resolution_clock::now(); \
    OUT_STREAM   << "BENCHMARK \"" << NAME << "\" : " \
                 << std::fixed << "CPU time: " \
                 << 1000.0 * (c_end-c_start) / CLOCKS_PER_SEC << " ms; " \
                 << "Wall time: " \
                 << std::chrono::duration<double, std::milli>(t_end-t_start).count() \
                 << " ms\n";
#else
#define BENCHMARK_START 
#define BENCHMARK_FINISH( OUT_STREAM, NAME )
#endif


using namespace std; //Very, very bad, but it's damn convenient for a small program

typedef uint16_t node_t;
typedef uint16_t lng_t;
typedef pair<pair<node_t, node_t>, lng_t> Edge;
typedef vector< Edge > Edges;

int readInput ( istream& is, Edges& res ) {
    int N; 
    if (cin >> N) res.reserve(N*N/2);
    int a,b,c;
    while (cin >> a >> b >> c) res.push_back(make_pair(make_pair(a,b),c));
    return N;
}

template<typename T>
class UnionFind {
    vector<T> roots;
    int N;
public:
    UnionFind(size_t size)
    : roots(size + 1), N(size) 
    {
       iota(begin(roots), end(roots), 0); 
    }
    int number() const { return N; }
    T find(T i) const { return roots[i];}
    void merge(T i ,T j) {
        if (roots[i] == roots[j]) return;
        const T i_root = roots[i];
        const T j_root = roots[j];
        replace(begin(roots), end(roots), i_root, j_root);
        N--;
    }
};

lng_t cluster(int N, Edges& edges, int K) {
    const size_t M = edges.size();
    sort(begin(edges), end(edges), [](const Edge& a, const Edge& b) -> bool
    {
        return a.second < b.second;
    });
    UnionFind<node_t> UF(N);
    map<int, lng_t> PQ;
    int i = 0;
    while (UF.number() > K && i < M) {
        const Edge& e = edges[i++];
        #ifdef DEBUG
        cerr << i <<" " << e.first.first << " " << e.first.second << " " << e.second << " " << UF.number() << " " << PQ.size() << endl;
        #endif
        UF.merge(e.first.first, e.first.second);
    }
    while ( i < M /*&& PQ.size() < 6*/ ) {
        const Edge& e = edges[i++];
        #ifdef DEBUG
        cerr << i <<" " << e.first.first << " " << e.first.second << " " << e.second << " " << UF.number() << " " << PQ.size() << endl;
        #endif
        node_t cl1 = UF.find(e.first.first);
        node_t cl2 = UF.find(e.first.second);
        if (cl1 != cl2) {
            int hash = (cl1 + cl2)/2*(cl1 + cl2 + 1); //Cantor pairing function
            #ifdef DEBUG
            cerr << "\t" << cl1 << " " << cl2 << " " <<hash << endl;
            #endif
            if (PQ[hash] == 0 || PQ[hash] > e.second) PQ[hash] = e.second;
        }
    }
    return max_element(begin(PQ), end(PQ), [](auto a, auto b) {return a.second > b.second;})->second;
}

int main(int argc, char** argv) {
    try {
        if (argc != 2) throw invalid_argument("The number of calusters must be passed as a command-line argument");
        Edges edges;
        //file read
        BENCHMARK_START
        int N = readInput( cin, edges );
        BENCHMARK_FINISH( cerr, "read file" )
        #ifdef DEBUG
        cerr << "DUBUG: " << N << " " << edges.size() << endl;
        #endif
        //clustering
        BENCHMARK_START
        lng_t dist = cluster(N, edges, atoi(argv[1]));
        BENCHMARK_FINISH( cerr, "clustering" )
        cout << dist << endl;
    } catch(exception& e) {
        cerr << "Error : " << e.what() << endl;
        return 1;
    }
    return 0;
}
