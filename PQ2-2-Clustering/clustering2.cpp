/* 
 * Find shortest pathst on a unordered weighted graph 
 * Copyleft 2015  Alexey Novikov <alexey@novikov.io>
 */ 

#include <iostream>
#include <iomanip>
#include <string>
#include <sstream>
#include <vector>
#include <deque>
#include <numeric>
#include <cassert>
#include <algorithm>
#include <iterator>
#include <cmath>
#include <set>
#include <queue>
#include <map>
#include <bitset>

#ifdef BENCH
#include <ctime>
#include <chrono>
    static std::chrono::high_resolution_clock::time_point t_start, t_end;
    std::clock_t c_start, c_end;
#define BENCHMARK_START \
    t_start = std::chrono::high_resolution_clock::now(); \
    c_start = std::clock();
#define BENCHMARK_FINISH( OUT_STREAM, NAME ) \
    c_end = std::clock(); \
    t_end = std::chrono::high_resolution_clock::now(); \
    OUT_STREAM   << "BENCHMARK \"" << NAME << "\" : " \
                 << std::fixed << "CPU time: " \
                 << 1000.0 * (c_end-c_start) / CLOCKS_PER_SEC << " ms; " \
                 << "Wall time: " \
                 << std::chrono::duration<double, std::milli>(t_end-t_start).count() \
                 << " ms\n";
#else
#define BENCHMARK_START 
#define BENCHMARK_FINISH( OUT_STREAM, NAME )
#endif


using namespace std; //Very, very bad, but it's damn convenient for a small program

static const size_t BITS_NUM = 24;
static const int OCCUPATED = -1;
static const int FREE = -2;

int readInput ( istream& is, vector<int>& buckets  ) {
    int N, bits; 
    if (!(cin >> N >> bits)) return -1;
    if (bits > BITS_NUM) throw runtime_error("wrong bitfield length");
    string str;
    getline(cin, str);
    buckets.clear();
    buckets.resize(pow(2, bits), FREE);
    while (getline(cin, str)) {
        str.erase(std::remove(str.begin(), str.end(), ' '), str.end());
        bitset<BITS_NUM> bitstr(str);
        buckets[bitstr.to_ulong()] = OCCUPATED;
    }
    return N;
}

template<typename T>
class UnionFind {
    vector<T> roots;
    int N;
public:
    UnionFind(size_t size)
    : roots(size), N(size) 
    {
       iota(begin(roots), end(roots), 0); 
    }
    int number() const { return N; }
    T find(T i) const { return roots[i];}
    void merge(T i ,T j) {
        if (roots[i] == roots[j]) return;
        const T i_root = roots[i];
        const T j_root = roots[j];
        replace(begin(roots), end(roots), i_root, j_root);
        N--;
    }
};

int main(int argc, char** argv) {
    try {
        vector<int> buckets;
        //file read
        BENCHMARK_START
        int N = readInput( cin, buckets );
        const size_t M = buckets.size();
        BENCHMARK_FINISH( cerr, "read file" )
        #ifdef DEBUG
        cerr << "DUBUG: " << N << " " << buckets.size() << endl;
        #endif
        //clustering
        BENCHMARK_START
        int c = 0;
        UnionFind<int> uf(N);
        for(int i = 0; i < M; i++) {
            if ( buckets[i] != FREE ) {
                if (buckets[i] == OCCUPATED ) buckets[i] = c;
                else uf.merge(buckets[i], c);
                //touch all succsiding neighbours in cycle
                for ( uint j1 = 0; j1 < BITS_NUM - 1; j1++ ) {
                    for ( uint j2 = j1 + 1; j2 < BITS_NUM; j2++ ) {
                        uint j = i ^ (1 << j1) ^ (1 << j2);
                        if ( buckets[j] == FREE ) continue;
                        if (buckets[j] == OCCUPATED ) buckets[j] = c;
                        else uf.merge(buckets[j], c);
                    }
                }
                #ifdef DEBUG
                cerr  << "\b\b\b\b\b\b" << c;
                #endif
                c++;
            }
        }
        #ifdef DEBUG
        cerr  << "\b\b\b\b\b\b";
        #endif
        BENCHMARK_FINISH( cerr, "clustering" )
        cout << uf.number() << endl;
    } catch(exception& e) {
        cerr << "Error : " << e.what() << endl;
        return 1;
    }
    return 0;
}

