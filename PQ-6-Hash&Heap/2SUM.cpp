/* 
 * 2-SUM 
 * Copyleft 2015  Alexey Novikov <alexey@novikov.io>
 */ 

#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <deque>
#include <numeric>
#include <cassert>
#include <algorithm>
#include <iterator>
#include <cmath>
#include <set>
#include <unordered_set>
#include <unordered_map>
#include <map>
#include <list>

#ifdef BENCH
#include <ctime>
#include <chrono>
    static std::chrono::high_resolution_clock::time_point t_start, t_end;
    std::clock_t c_start, c_end;
#define BENCHMARK_START \
    t_start = std::chrono::high_resolution_clock::now(); \
    c_start = std::clock();
#define BENCHMARK_FINISH( OUT_STREAM, NAME ) \
    c_end = std::clock(); \
    t_end = std::chrono::high_resolution_clock::now(); \
    OUT_STREAM   << "BENCHMARK \"" << NAME << "\" : " \
                 << std::fixed << "CPU time: " \
                 << 1000.0 * (c_end-c_start) / CLOCKS_PER_SEC << " ms; " \
                 << "Wall time: " \
                 << std::chrono::duration<double, std::milli>(t_end-t_start).count() \
                 << " ms\n";
#else
#define BENCHMARK_START 
#define BENCHMARK_FINISH( OUT_STREAM, NAME )
#endif


using namespace std; //Very, very bad, but it's damn convenient for a small program

typedef int64_t data_t;
typedef set< data_t > DataContainer;

template < typename hashT >
string hashStats ( const hashT & hash, string name ) {
    ostringstream os;
    size_t height = 0;
    map< size_t, size_t > stat;
    for( int i = 0; i<hash.bucket_count(); i++ ) {
        size_t sz = hash.bucket_size(i);
        height = max(height, sz);
        stat[sz]++;
        }
    os << name << " :";
    os << "\tsize: " << hash.size() << "\tload_factor :" << hash.load_factor() << "\theight: " << height << "\twidth : "
    << hash.bucket_count() << "\tstat : ";
    for ( int i = 0; i <= height; i++ ) os << i << " : " << stat[i] << " ";
    os << endl;
    return os.str();
}
    

int main(int argc, char* argv[])
{
    if ( argc < 3 ) {
        cerr << "Too few arguments" << endl;
        return 1;
    }
    const data_t tMIN  = stoi(*(++argv));
    const data_t tMAX  = stoi(*(++argv));
    const data_t tCENTER = (tMAX + tMIN)/2;
    const data_t tRADIUS = tMAX - tCENTER;
    const float max_load_factor = (argc > 3) ? (stof(*++argv)) : (0.75);
    if (tCENTER != 0) {
        //We'd need a couple of minor changes to support this case
        cerr << "Must be centered on zero" << endl;
        return -1;
    }
    try {
        deque< data_t > input;
        data_t pmin = numeric_limits<data_t>::max();
        data_t pmax = 0;
        data_t nmin = 0;
        data_t nmax = numeric_limits<data_t>::min();
        data_t t;
        int count = 0;
        BENCHMARK_START
            while (cin >> t) {
                input.push_back(t);
                if (t >=0 ) {
                    pmin = min(pmin, t);
                    pmax = max(pmax, t);
                } else {
                    nmin = min(nmin, t);
                    nmax = max(nmax, t);
                }
            }
        BENCHMARK_FINISH( cerr, "read file" )       
        #ifdef DEBUG
            cerr << " [ " << nmin << " , " << nmax << " ] " << " - " << " [ " << pmin << " , " << pmax << " ] " << endl;  
        #endif

#ifndef NAIVE
        unordered_map< uint, DataContainer > positives, negatives;
        positives.max_load_factor(max_load_factor);
        negatives.max_load_factor(max_load_factor);
#endif
        unordered_set< data_t > center;   
        center.max_load_factor(max_load_factor);
        
        list< data_t > tList (tMAX - tMIN);
        iota(tList.begin(), tList.end(), tMIN);

        BENCHMARK_START
            for ( data_t t : input ) {
#ifdef NAIVE
                center.insert(t);
#else
                //We split the set of numbers into negative (-inf, -tMIN ), 
                //positive ( tMAX, +inf ) and center [tMIX, tMAX] parts
                //x and y might be in the center part both. 
                //Otherwise we assume x to be in positive part and y to be in the negative
                //Let's split the set of numbers given into the buckets of width tRADIUS      
                int bucket = t / tRADIUS;
                if ( t > 0 )    positives[bucket].insert(t);
                else if (t < 0) negatives[-bucket].insert(t);
                if ( t >= tMIN && t<= tMAX ) center.insert(t); // two central buckets is a special case
                                                               
#endif
                }
        BENCHMARK_FINISH( cerr, "Create" )
#ifdef STATS
#ifndef NAIVE
        cerr << hashStats(positives, "Positives");
        cerr << hashStats(negatives, "Negatives");
#endif
        cerr << hashStats(center, "Center");
#endif
        BENCHMARK_START
#ifndef NAIVE    //Intelligent algorithm  ~ O ( (number of 2SUMs) * (tMAX - tMIN) + n )
            for ( pair < uint, DataContainer > cont : positives ) {
                auto twin =  negatives.find( cont.first );
                auto pretwin = negatives.find( cont.first-1 );
                auto postwin = negatives.find( cont.first+1 );
                if ( pretwin == negatives.end() && twin == negatives.end() 
                    && postwin == negatives.end() )continue; 
                for ( data_t x : cont.second ) {
                    auto tit = tList.begin();
                    while ( tit != tList.end() ) {
                        data_t y = *tit - x;
                        if (  ( pretwin != negatives.end() && pretwin->second.find(y) != pretwin->second.end())
                           || ( twin != negatives.end() && twin->second.find(y) != twin->second.end() )
                           || ( postwin != negatives.end() && postwin->second.find(y) != postwin->second.end()) 
                           ) {
                           count++;
                            #ifdef DEBUG
                                cerr << "\t2SUM: " << x << " " << y << " " << *tit << endl;
                            #endif
                           tit = tList.erase(tit);
                        } else tit++;
                    }
                }
            }
#endif      //Brute-force algorithm ~ O( n* (tMAX - tMIN) )
            for ( data_t x : center ) {
                auto tit = tList.begin();
                while ( tit != tList.end() ) {
                    data_t y = *tit - x;
                    if ( center.find(y) != center.end() ) {
                        count++;
                        #ifdef DEBUG
                            cerr << "\t2SUM: " << x << " " << y << " " << *tit << endl;
                        #endif
                        tit = tList.erase(tit);
                    } else tit++;
                }
            }
        BENCHMARK_FINISH( cerr, "Count" )
        cout << count;
        cout << endl;
        return 0;
    } catch(exception& e) {
        cerr << "Error : " << e.what() << endl;
        return 1;
    }
}

