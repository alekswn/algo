/* 
 * Find shortest pathst on a unordered weighted graph 
 * Copyleft 2015  Alexey Novikov <alexey@novikov.io>
 */ 

#include <iostream>
#include <iomanip>
#include <string>
#include <sstream>
#include <vector>
#include <deque>
#include <numeric>
#include <cassert>
#include <algorithm>
#include <iterator>
#include <cmath>
#include <set>
#include <queue>

#ifdef BENCH
#include <ctime>
#include <chrono>
    static std::chrono::high_resolution_clock::time_point t_start, t_end;
    std::clock_t c_start, c_end;
#define BENCHMARK_START \
    t_start = std::chrono::high_resolution_clock::now(); \
    c_start = std::clock();
#define BENCHMARK_FINISH( OUT_STREAM, NAME ) \
    c_end = std::clock(); \
    t_end = std::chrono::high_resolution_clock::now(); \
    OUT_STREAM   << "BENCHMARK \"" << NAME << "\" : " \
                 << std::fixed << "CPU time: " \
                 << 1000.0 * (c_end-c_start) / CLOCKS_PER_SEC << " ms; " \
                 << "Wall time: " \
                 << std::chrono::duration<double, std::milli>(t_end-t_start).count() \
                 << " ms\n";
#else
#define BENCHMARK_START 
#define BENCHMARK_FINISH( OUT_STREAM, NAME )
#endif


using namespace std; //Very, very bad, but it's damn convenient for a small program

typedef uint16_t data_t;
typedef deque< data_t > Input;
static const data_t FRAME = 10000;


Input readInput ( istream& is ) {
    deque< data_t > res;
    data_t t;
    while (cin >> t) res.push_back(t);
    return res;
}

data_t medSumPQ ( const Input& input ) {
    priority_queue<data_t, vector <data_t>, greater<data_t> > pqH; 
    priority_queue<data_t, vector <data_t>, less<data_t> > pqL; 
    data_t pqsum = 0;
    for ( data_t t : input ) {
        if (pqL.size() == pqH.size()) { //Total number of entries is even 
           if ( !pqL.empty() && t > pqH.top() ) {
                pqL.push(pqH.top());
                pqH.pop();
                pqH.push(t);
           } else pqL.push(t);
        } else {
            if ( t > pqL.top() ) pqH.push(t);
            else {
                pqH.push(pqL.top());
                pqL.pop();
                pqL.push(t);
            }
        }
        pqsum = ( pqsum + pqL.top() ) % FRAME;
#ifdef DEBUG 
        cerr << pqL.top() << " " << setw(4) << pqsum << endl;
#endif
    }
    return pqsum;
}

data_t medSumTree ( const Input& input ) {
    set<data_t, less<data_t> > right;
    set<data_t, greater<data_t> > left; 
    data_t sum = 0;
    for ( data_t t : input ) {
        if (left.size() == right.size()) { //Total number of entries is even 
           if ( !left.empty() && t > *right.begin() ) {
                left.insert(*right.begin());
                right.erase(right.begin());
                right.insert(t);
            } else left.insert(t);
        } else {
            if ( t > *left.begin() ) right.insert(t);
            else {
                right.insert(*left.begin());
                left.erase(left.begin());
                left.insert(t);
            }
        }
        data_t median = *left.begin();
        sum = ( sum + median ) % FRAME;
#ifdef DEBUG 
        cerr << median << " "  << setw(4) << sum << endl;
#endif
    }
    return sum;
}

int main() {
    try {
        //file read
        BENCHMARK_START
        Input input = readInput( cin );
        BENCHMARK_FINISH( cerr, "read file" )
        
        //Heap
        BENCHMARK_START
        data_t sumPQ = medSumPQ ( input );
        BENCHMARK_FINISH( cerr, "Heap" )

        BENCHMARK_START
        data_t sumTree = medSumTree( input );
        BENCHMARK_FINISH( cerr, "Tree" )

        cout << sumPQ << endl << sumTree << endl;       

    } catch(exception& e) {
        cerr << "Error : " << e.what() << endl;
        return 1;
    }
    return 0;
}
