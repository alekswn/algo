/* 

 * Copyleft 2016  Alexey Novikov <alexey@novikov.io>
 */ 

#include <iostream>
#include <iomanip>
#include <string>
#include <sstream>
#include <vector>
#include <deque>
#include <numeric>
#include <cassert>
#include <algorithm>
#include <iterator>
#include <cmath>
#include <queue>
#include <map>
#include <bitset>
#include <functional>

#ifdef BENCH
#include <ctime>
#include <chrono>
    static std::chrono::high_resolution_clock::time_point t_start, t_end;
    std::clock_t c_start, c_end;
#define BENCHMARK_START \
    t_start = std::chrono::high_resolution_clock::now(); \
    c_start = std::clock();
#define BENCHMARK_FINISH( OUT_STREAM, NAME ) \
    c_end = std::clock(); \
    t_end = std::chrono::high_resolution_clock::now(); \
    OUT_STREAM   << "BENCHMARK \"" << NAME << "\" : " \
                 << std::fixed << "CPU time: " \
                 << 1000.0 * (c_end-c_start) / CLOCKS_PER_SEC << " ms; " \
                 << "Wall time: " \
                 << std::chrono::duration<double, std::milli>(t_end-t_start).count() \
                 << " ms\n";
#else
#define BENCHMARK_START 
#define BENCHMARK_FINISH( OUT_STREAM, NAME )
#endif

typedef int32_t value_t;
typedef int32_t weight_t;

typedef std::pair<value_t, weight_t> item_t;

using namespace std; //Very, very bad, but it's damn convenient for a small program

int readInput ( istream& is, vector<item_t>& items) {
    uint N, W; 
    if (!(cin >> W >> N)) return -1;
    items.clear();
    items.reserve(N);
    int v, w;
    while (cin >> v >> w) {
        items.push_back(make_pair(v,w));
    }
    if ( items.size() != N )
        throw runtime_error("Bad input");
    return W;
}

value_t knapsack_naive(int W, vector<item_t>& items) {
    vector<value_t> Ai_1(W + 1, 0);
    vector<value_t> Ai(W + 1);
    
    for_each(begin(items), end(items), [&](const item_t& item) {
        for (weight_t x = 1; x <= W; x++) {
            const weight_t x_w = x - item.second;
            if (x_w >= 0) Ai[x] = max(Ai_1[x], Ai_1[x_w] + item.first);
            else Ai[x] = Ai_1[x];
            #ifdef DEBUG
            cerr << "\t" << Ai[x];
            #endif
        }
        #ifdef DEBUG
        cerr << endl;
        #endif
        swap(Ai, Ai_1);
    });
    return Ai_1.back();
}
value_t knapsack_fast(int W,const vector<item_t>& items) {
    map<weight_t, value_t, std::greater<weight_t>> A;
    A.insert(make_pair(0,0));
    for_each(begin(items), end(items), [&](const item_t& item) {
        const int v = item.first;
        const int w = item.second;
        if ( w > W ) return;
        #ifdef DEBUG
            cerr << item.first << "," << item.second << " " << A.begin()->first << " " << A.rbegin()->first << "|\t";
        #endif
        
        for(auto wit = begin(A); wit != end(A); wit++) {
            if (wit->first + w <= W)
            if (v + wit->second > A.lower_bound(wit->first + w)->second) {
                A[wit->first + w] = v + wit->second;
            }
            if (wit->first >= w ) {
                wit->second = max(wit->second, 
                                    v + A.lower_bound(wit->first - w)->second);
                if (wit->second == prev(wit)->second) wit = A.erase(prev(wit));
            }
        }
/*
        //remove sucessive dublicate values
        auto cmp = [](const pair<weight_t, value_t>& a, 
                  const pair<weight_t, value_t>& b) -> bool {
                                           return a.second == b.second; 
                                       };
        for (auto it = adjacent_find( rbegin(A), rend(A), cmp); it != A.rend(); 
            it = adjacent_find( it, rend(A), cmp)) {
                it = reverse_iterator<map<weight_t, value_t, std::greater<weight_t>>::iterator> (A.erase(it.base()));
            } 
*/

        #ifdef DEBUG
        for_each(rbegin(A), rend(A), [](auto e) {
            cerr << e.first << "," << e.second << "\t";
        });
        cerr << endl;
        #endif
    });

    return A.lower_bound(W)->second;
}

int main(int argc, char** argv) {
    try {
        vector<item_t> items;
        //file read
        BENCHMARK_START
        weight_t W = readInput( cin, items );
        BENCHMARK_FINISH( cerr, "read file" )
        cout << "SIZE: " << W  << " " << items.size() << endl;
        if ( W < 20000 ) {
            //naive
            BENCHMARK_START
            value_t val  = knapsack_naive(W, items);
            BENCHMARK_FINISH( cerr, "naive" )
            cout << "NAIVE: " <<val << endl;
        }
        //fast
        BENCHMARK_START
        value_t val  = knapsack_fast(W, items);
        BENCHMARK_FINISH( cerr, "fast" )
        cout << "FAST: " <<val << endl;
    } catch(exception& e) {
        cerr << "Error : " << e.what() << endl;
        return 1;
    }
    return 0;
}

