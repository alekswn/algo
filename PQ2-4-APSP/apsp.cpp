/* 
 * Find all shortes paths on an ordered weighted graph 
 * Copyleft 2016  Alexey Novikov <alexey@novikov.io>
 */ 

#include <algorithm>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <deque>
#include <numeric>
#include <random>
#include <cassert>
#include <algorithm>
#include <iterator>
#include <cmath>
#include <limits>
#include <exception>
#include <list>
#include <thread>

#ifdef BENCH
#include <ctime>
#include <chrono>
    static std::chrono::high_resolution_clock::time_point t_start, t_end;
    std::clock_t c_start, c_end;
#define BENCHMARK_START \
    t_start = std::chrono::high_resolution_clock::now(); \
    c_start = std::clock();
#define BENCHMARK_FINISH( OUT_STREAM, NAME ) \
    c_end = std::clock(); \
    t_end = std::chrono::high_resolution_clock::now(); \
    OUT_STREAM   << "BENCHMARK \"" << NAME << "\" : " \
                 << std::fixed << "CPU time: " \
                 << 1000.0 * (c_end-c_start) / CLOCKS_PER_SEC << " ms; " \
                 << "Wall time: " \
                 << std::chrono::duration<double, std::milli>(t_end-t_start).count() \
                 << " ms\n";
#else
#define BENCHMARK_START 
#define BENCHMARK_FINISH( OUT_STREAM, NAME )
#endif


using namespace std; //Very, very bad, but it's damn convenient for a small program

typedef uint32_t index_t;
typedef int32_t weight_t;

static const weight_t INF_WEIGHT = numeric_limits<weight_t>::max(); 
static const index_t  FAKE_ROOT  = 0;

class Graph
{
protected:
    struct Edge {
        weight_t weight;
        mutable weight_t adjustedWeight;
        index_t end;
        Edge() : weight(INF_WEIGHT), adjustedWeight(INF_WEIGHT), end(FAKE_ROOT) {};
        Edge(index_t e, weight_t w) :weight(w), adjustedWeight(INF_WEIGHT), end(e){};
    };
    typedef list< Edge > Edges;
    struct Vertex {
        Edges in, out;
        mutable weight_t adjustedWeight = INF_WEIGHT;
    };
    typedef vector< Vertex > Vertexes;
private:
    Vertexes vertexes;
    mutable bool inited = false;
    mutable bool negCycle;

public:
    static Graph readGraph( istream& s ); 

    size_t first() const noexcept { return FAKE_ROOT + 1; };
    size_t last() const noexcept { return vertexes.size() - 1; };
    string dump( bool adjusted = false ) const;
    
    bool hasNegativeCycle() const { 
        if (!inited) throw runtime_error("Graph was not initiated"); 
        return negCycle; 
    };
/*
    vector< weight_t > dijkstra_naive( index_t s ) const;
    vector< weight_t > dijkstra_better ( index_t s ) const;
    vector< weight_t > dijkstra_heap ( index_t s ) const;
*/
    vector< weight_t > bellman_ford ( index_t s ) const;
    weight_t minAPSP_bf() const;
    
    //TODO implement Jonson's algorithm
    //TODO implement Floyd-Warshall
protected:
    Graph(size_t N) : vertexes(N) {};
    bool reweight() const;
    size_t size() const noexcept { return vertexes.size(); };
    weight_t minAPSP_bf_worker(index_t start, index_t finish, weight_t& minPath) const;

    void init();
};

void Graph::init() {
    vertexes[FAKE_ROOT].in.clear();
    vertexes[FAKE_ROOT].out.clear();
    for ( index_t v = first(); v <= last(); v++ ) {
        vertexes[FAKE_ROOT].out.emplace_back(v,0);
        vertexes[v].in.emplace_back(FAKE_ROOT,0);
    }
    negCycle = !reweight();
    inited = true;
}

Graph Graph::readGraph( istream& s ) 
{
    index_t N,M;
    if ( !(s >> N >> M) ) 
        throw runtime_error("Bad input: graph dimentions expected");
    Graph G(N+1);
    index_t u,v;
    weight_t w;
    while (s >> u >> v >> w) {
        if (u > N || v > N) runtime_error("Bad input: invalid vertex");
        M--;
        G.vertexes[u].out.emplace_back(v,w);
        G.vertexes[v].in.emplace_back(u,w);
    }
    if (M != 0) throw runtime_error("Bad input: wrong number of edges");
    G.init();
    return std::move(G);
}

string Graph::dump(bool adjusted) const
{
    ostringstream os;
    //matrix representation of a graph
    vector< vector< weight_t > > matrix( size(), vector< weight_t >(size(), INF_WEIGHT));

    if (!adjusted) {
        for( index_t i = first(); i <= last(); i++ )
            for( const Edge& edge : vertexes[i].out )
                matrix[i][edge.end] = edge.weight;
    } else {
        for( index_t i = first(); i <= last(); i++  ) {
            matrix[i][i] = vertexes[i].adjustedWeight;
            for( const Edge& edge : vertexes[i].out ) 
                matrix[i][edge.end] = edge.adjustedWeight;
        }
    }

    os.width(8); os << '+';
    for (index_t i = first(); i <= last(); i++ ) {os.width(8); os << i;}
    os << endl;
    for (index_t i = first(); i <= last(); i++) {
        const vector< weight_t >& line = matrix[i];
        os.width(8); os << i;
        for (index_t j = first(); j <= last(); j++) {
            weight_t w = line[j];
            os.width(8); 
            (w != INF_WEIGHT) ? os << w : os << "@";
        }
        os << endl;
    }
    return os.str();
}
/*
vector< weight_t > Graph::dijkstra_naive( index_t s ) const
{
    vector < weight_t > A(size(), MAX_DST);
    vector < bool > X(size(), false);

    A[s] = 0;
    weight_t min_dst;
  
    do {
        min_dst = MAX_DST;
        for( index_t v = 0; v < size(); v++ ) {
            if (X[v] == true || A[v] == MAX_DST) continue;
            for ( Edge edge : vertexes[v] ) {
                weight_t dst = A[v] + edge.second;
                if (dst < A[v]) A[v] = dst;
                if (A[v] < min_dst) {
                    min_dst = A[v];
                    s = v;
                    X[s] = true;
                }
            #ifdef DEBUG
                cerr << "\t" << __PRETTY_FUNCTION__ << " : " << (int)v << " " << (int)edge.first << " " << dst << " | " << (int)s << " " << min_dst  << endl;
            #endif
            }
        }
        #ifdef DEBUG
            cerr << __PRETTY_FUNCTION__ << " : " << (int)s << " " << min_dst << endl;
        #endif
    } while (min_dst < MAX_DST); //Remaining vertexes are not connected
    return A;
}

vector< weight_t > Graph::dijkstra_better( index_t s ) const
{
    vector < weight_t > A(size(), MAX_DST);
    vector < bool > X(size(), false);
    typedef pair < index_t, Edge > Candidat;
    typedef unordered_multimap < index_t, Edge > Candidates;
    Candidates candidates;

    A[s] = 0;
    weight_t min_dst;
    for (Edge edge: vertexes[s])
        candidates.emplace( edge.first, make_pair( s, edge.second ) );
  
    while (!candidates.empty()) {
        X[s] = true;
        min_dst = MAX_DST;
        for( Candidat c : candidates ) {
            weight_t dst = A[c.second.first] + c.second.second;
            A[c.first] = min(A[c.first], dst);
            if (A[c.first] < min_dst) min_dst = A[s = c.first];
            #ifdef DEBUG
                cerr << "\t" << __PRETTY_FUNCTION__ << " : " << (int)c.first << " " << (int)c.second.first << " " << dst << " | " << (int)s << " " << min_dst  << endl;
            #endif
        }
        candidates.erase(s);
        for (Edge edge: vertexes[s])
            if (X[edge.first] == false)
                candidates.emplace( edge.first, make_pair( s, edge.second ) );
        #ifdef DEBUG
            cerr << __PRETTY_FUNCTION__ << " : " << (int)s << " " << min_dst << endl;
        #endif
    }
    return A;
}

vector< weight_t > Graph::dijkstra_heap( index_t s ) const
{
    vector < weight_t > A(size(), MAX_DST);
    vector < bool > X(size(), false);
    
    class VX {
        vector < pair< weight_t, index_t > > heap;
        vector < index_t > indx;
        size_t heap_size;
    public:
        VX( size_t size, weight_t def_val ) 
            : heap_size(0)
        {
            heap.reserve(size);
            indx.reserve(size);
            for ( int i = 0; i < size; i++ ) {
                heap.push_back(make_pair( def_val, i ) );
                indx.push_back(i);
            }
        }
        void set( index_t i, weight_t v ) {
            heap[indx[i]].first = v;
            if (indx[i] >= heap_size) swap(indx[i], heap_size++);
            else down(indx[i]);
            up(indx[i]);
        }
        void pop() {
            swap(0, --heap_size);
            down(0);
        }
        weight_t get( index_t i ) const { return heap[indx[i]].first; }
        index_t  top() const { return heap.front().second; }
        weight_t  top_val() const { return heap.front().first; }
    private:
        void swap ( index_t i, index_t j ) {
            std::swap(heap[i], heap[i]);
            std::swap(indx[heap[i].second], indx[heap[j].second]);
        }
        void up (index_t i) {
            index_t p = parent(i);
            if (heap[p] <= heap[i]) return;
            swap( p, i );
            up(p);
        }
        void down (index_t i) {
            index_t c = succ(i);
            if (heap[c] >= heap[i]) return;
            swap( c, i );
            down(c);
        }
        index_t parent( index_t i ) { return ((i+1)>>1)-1; }
        index_t succ( index_t i ) {
            i++;
            index_t l = i<<1;
            index_t r = l+1;
            return (heap[l] <= heap[r]) ? (l) : (r);
        }
    } vx(size(), MAX_DST);     
    vx.set(s,0);
    weight_t min_dst;
    do {
        s = vx.top();
        A[s] = vx.top_val();
        vx.pop();
        for ( Edge edge : vertexes[s] ) {
             weight_t dst = A[s] + edge.second;
             if (dst < vx.get( edge.first )) {
                 vx.set(edge.first, dst);
                #ifdef DEBUG
                    cerr << "\t" << __PRETTY_FUNCTION__ << " : " << (int)s << " " << (int)edge.first << " " << dst  << endl;
                #endif
             }
        }
        #ifdef DEBUG
            cerr << __PRETTY_FUNCTION__ << " : " << (int)vx.top() << " " << vx.top_val()  << endl;
        #endif
    } while (vx.top_val() != MAX_DST); //Remaining vertexes are not connected
    return A;
}
*/
vector< weight_t > Graph::bellman_ford( index_t s ) const
{
    const size_t N = size();
    vector< weight_t > Ai(N, INF_WEIGHT);
    vector< weight_t > Ai_1(N, INF_WEIGHT);

    Ai_1[s] = 0;    
    for (size_t i = 1; i <= size(); i++) {
        for ( index_t w = 0; w < N; w++ ) {
            weight_t dst = INF_WEIGHT;
            for ( const Edge& e : vertexes[w].in ) {
                index_t v = e.end;
                if (Ai_1[v] != INF_WEIGHT)
                    dst = min(dst, Ai_1[v] + e.weight);
            }
            Ai[w] = min(Ai_1[w], dst);
            #ifdef DEBUG
            cerr << '\t' << (int)w << " " << Ai[w] << endl;
            #endif
        }
        swap(Ai, Ai_1);
    }

    //If the last run was not dry there is a negative cycle.
    if (Ai != Ai_1) return vector< weight_t >();
    Ai[s] = INF_WEIGHT;
    return Ai;
}

bool Graph::reweight() const {
    vector<weight_t> st = bellman_ford(FAKE_ROOT);
    if (st.empty()) return false;
    for (size_t i = first(); i <= last(); i++) {
        if (st[i] == INF_WEIGHT) continue;
        vertexes[i].adjustedWeight = st[i];
        for (const Edge& e: vertexes[i].in) {
            if (st[e.end] == INF_WEIGHT || e.weight == INF_WEIGHT) continue;
            e.adjustedWeight = e.weight + st[e.end] - st[i];
        }
        for (const Edge& e: vertexes[i].out) {
            if (st[e.end] == INF_WEIGHT || e.weight == INF_WEIGHT) continue;
            e.adjustedWeight = e.weight + st[i] - st[e.end];
        }
    }
    return true;
}

weight_t Graph::minAPSP_bf_worker(index_t start, index_t finish, weight_t& minPath) const {
    if (hasNegativeCycle()) throw runtime_error("Graph has negative cycle");
    minPath = INF_WEIGHT;
    for ( index_t i = start; i < finish; i++ ) {
        auto spsp = bellman_ford(i);
        minPath = min(minPath, *min_element(begin(spsp), end(spsp)));
    }
    return minPath;
}

weight_t Graph::minAPSP_bf() const {
    vector<thread> thread_pool;
    int my_result = INF_WEIGHT;
    const uint tasks_num = last() - first() + 1;
    //We'll do some amount of work on the current thread.
    const uint threads_num = min(thread::hardware_concurrency(), tasks_num) - 1;
    const uint tasks_per_thread = tasks_num / threads_num;
    if (threads_num <= 0) return minAPSP_bf_worker( first(), last(), my_result );
    #ifdef DEBUG
    cerr << "Starting " << threads_num << " threads" << endl;
    #endif
    thread_pool.reserve(threads_num);
    vector<weight_t> results(threads_num); 
    for (uint i = 0; i < threads_num; i++) {
        thread_pool.emplace_back( &Graph::minAPSP_bf_worker, this, i*tasks_per_thread, (i+1)*tasks_per_thread - 1, ref(results[i]));
    }
    const int tasks_rem = tasks_num - threads_num * tasks_per_thread;
    if (tasks_rem > 0) minAPSP_bf_worker( threads_num * tasks_per_thread, last(), my_result );
    
    for (thread& t : thread_pool) t.join();
    
    return min(my_result, *min_element(begin(results), end(results)));
}

int main(int argc, char* argv[])
{
    try {

        BENCHMARK_START
        Graph graph = Graph::readGraph(cin);
        BENCHMARK_FINISH( cerr, "readGraph" )       
        #ifdef DEBUG
            cerr <<  graph.dump(false) << endl;
            cerr <<  graph.dump(true) << endl;
        #endif
/*
        BENCHMARK_START
            vector< weight_t > A = graph.dijkstra_naive(s);
        BENCHMARK_FINISH( cerr, "Dijkstra naive" )
        #ifdef DEBUG
            cerr << "Dijkstra naive:" << endl; 
            for ( int i = 0; i < A.size(); i++ )
                cerr << "\t" << i << "\t" << A[i] << endl;
        #endif
        for (index_t t : tt) {
            if ( t >= graph.size() ) throw range_error("Invalid dest vertex");
            cout << A[t] << ",";
        }
        cout << '\b';
        cout << endl;
        BENCHMARK_START
            vector< weight_t > Ab = graph.dijkstra_better(s);
        BENCHMARK_FINISH( cerr, "Dijkstra better" )
        #ifdef DEBUG
            cerr << "Dijkstra better:" << endl; 
            for ( int i = 0; i < Ab.size(); i++ )
                cerr << "\t" << i << "\t" << Ab[i] << endl;
        #endif
        for (index_t t : tt) {
            cout << Ab[t] << ",";
        }
        cout << '\b';
        cout << endl;
        BENCHMARK_START
            vector< weight_t > Ah = graph.dijkstra_heap(s);
        BENCHMARK_FINISH( cerr, "Dijkstra heap" )
        #ifdef DEBUG
            cerr << "Dijkstra heap:" << endl; 
            for ( int i = 0; i < Ah.size(); i++ )
                cerr << "\t" << i << "\t" << Ah[i] << endl;
        #endif
        for (index_t t : tt) {
            cout << Ah[t] << ",";
        }
        cout << '\b';
        cout << endl;
*/
        if (graph.hasNegativeCycle()) { 
            cout << "Negative cycle detected " << endl;
            return 0;
        }
        BENCHMARK_START
        weight_t minPath_bf = graph.minAPSP_bf();
        BENCHMARK_FINISH( cerr, "Bellman-Ford:" )
        cout << "Bellman-Ford: "<< minPath_bf << endl;
        return 0;
    } catch(exception& e) {
        cerr << "Error : " << e.what() << endl;
        return 1;
    }
}
