/* 
 * Count the number of comparation in quicksort 
 * Copyleft 2015  Alexey Novikov <alexey@novikov.io>
 */ 

#include <iostream>
#include <forward_list>
#include <string>
#include <sstream>
#include <vector>
#include <cassert>
using namespace std;

typedef uint32_t data_t;
typedef forward_list<data_t> List;
typedef vector<data_t> Array;

inline bool isInversed(data_t first, data_t second)
{
    return (first > second);
}

string dump(List& list)
{
    ostringstream os;
    cout << endl;
    int i = 0;
    for (auto it = list.begin(); it != list.end(); it++) os << i++ << " : " << *it << endl;
    return os.str();
}

string dump(Array::const_iterator begin, Array::const_iterator end)
{
    ostringstream os;
    int i = 0;
    for (auto it = begin; it != end; it++) os << *it << " ";
    return os.str();
}
inline string dump(Array arr) { return dump(arr.begin(), arr.end()); }

bool is_sorted (Array::const_iterator beg, Array::const_iterator end)
{
    for (auto prev = beg; ++beg != end;) if (isInversed(*(prev++), *beg)) return false;
    return true;
}
inline bool is_sorted (const Array& arr) { return is_sorted(arr.begin(), arr.end()); }


Array::iterator pivot_first (Array::iterator begin, Array::iterator end)
{
    return begin;
}
Array::iterator pivot_last (Array::iterator begin, Array::iterator end)
{
    if (begin == end) return end;
    return end-1;
}
Array::iterator pivot_3med (Array::iterator first, Array::iterator end)
{
    if (first == end) return end;
    auto last = end-1;
    auto med  = first + (distance(first, last) / 2);
    bool first_med = isInversed(*first, *med);
    bool med_last  = isInversed(*med, *last);
    if ( first_med == med_last ) return med;
    bool first_last  = isInversed(*first, *last);
    if ( first_last == !med_last ) return last;
    return first;
}

Array::iterator partition (Array::iterator begin, Array::iterator end, Array::iterator pivot)
{
    iter_swap(pivot, begin);
    auto i = begin + 1;
    for (auto j = i; j!= end; j++)
        if (isInversed(*begin, *j)) iter_swap(j, i++);
    iter_swap(begin, --i); 
    return i;
} 

int qsort_count (Array::iterator begin, Array::iterator end, 
		 Array::iterator(*getPivot)(Array::iterator, Array::iterator))
{  
    int size = distance(begin, end);
    assert(size >= 0);
#ifdef DEBUG
    cerr << size << " : " << dump(begin, end) << endl;
#endif
    if (size < 2) return 0;
    auto pivotIt = partition( begin, end, getPivot( begin, end ) );
    return --size + qsort_count( begin, pivotIt, getPivot) + qsort_count( pivotIt+1, end, getPivot);
}


int main()
{
    //Treat ',' as whitespace for cin
    const auto temp = ctype<char>::classic_table();
    vector<ctype<char>::mask> bar(temp, temp + ctype<char>::table_size);
    bar[','] |= ctype_base::space;
    cin.imbue(locale(cin.getloc(), new ctype<char>(bar.data())));
    
    List list;
    while (!cin.eof()) 
    {
        data_t i;
        if (cin >> i) {
		list.push_front(i);
	}
    }
    list.reverse();
    Array input_arr(list.begin(), list.end());
    Array sorting_arr(input_arr);
    cout << input_arr.size() << " numbers read. " << "Imput array is " 
		<< (is_sorted(input_arr) ? "sorted." : "unsorted")  << endl;
    
    cout << "Pivot first: " << qsort_count(sorting_arr.begin(), sorting_arr.end(), &pivot_first);
    cout << '\t' << (is_sorted(sorting_arr) ? " OK" : " FAIL") << endl;
#ifdef DEBUG
    cerr << '\t' << dump(sorting_arr) << endl;
#endif
    sorting_arr = input_arr;
    cout << "Pivot last : " << qsort_count(sorting_arr.begin(), sorting_arr.end(), &pivot_last);
    cout << '\t' << (is_sorted(sorting_arr) ? " OK" : " FAIL") << endl;
#ifdef DEBUG
    cerr << '\t' << dump(sorting_arr) << endl;
#endif
    sorting_arr = input_arr;
    cout << "Pivot 3-med: " << qsort_count(sorting_arr.begin(), sorting_arr.end(), &pivot_3med);
    cout << '\t' << (is_sorted(sorting_arr) ? " OK" : " FAIL") << endl;
#ifdef DEBUG
    cerr << '\t' << dump(sorting_arr) << endl;
#endif
    return 0;
}
        
    
