/* 
 * 2 - SAT problem
 * Copyleft 2016  Alexey Novikov <alexey@novikov.io>
 */ 

#include <algorithm>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <deque>
#include <numeric>
#include <random>
#include <cassert>
#include <algorithm>
#include <iterator>
#include <cmath>
#include <limits>
#include <exception>
#include <list>
#include <queue>
#include <forward_list>
#include <set>
#include <thread>


#ifdef BENCH
#include <ctime>
#include <chrono>
    static std::chrono::high_resolution_clock::time_point t_start, t_end;
    std::clock_t c_start, c_end;
#define BENCHMARK_START \
    t_start = std::chrono::high_resolution_clock::now(); \
    c_start = std::clock();
#define BENCHMARK_FINISH( OUT_STREAM, NAME ) \
    c_end = std::clock(); \
    t_end = std::chrono::high_resolution_clock::now(); \
    OUT_STREAM   << "BENCHMARK \"" << NAME << "\" : " \
                 << std::fixed << "CPU time: " \
                 << 1000.0 * (c_end-c_start) / CLOCKS_PER_SEC << " ms; " \
                 << "Wall time: " \
                 << std::chrono::duration<double, std::milli>(t_end-t_start).count() \
                 << " ms\n";
#else
#define BENCHMARK_START 
#define BENCHMARK_FINISH( OUT_STREAM, NAME )
#endif


using namespace std; //Very, very bad, but it's damn convenient for a small program

class Graph
{
public:
    //typedef priority_queue< size_t, std::deque< size_t >, std::less<size_t> > PQ;
    typedef set<size_t> SCC;
    typedef pair< size_t, size_t > Edge;
    typedef list< Edge > Edges;
    typedef vector< size_t > Order;
    typedef vector< bool > Flags;
private:
    typedef vector< list < size_t > > Vertexes;
    
    Vertexes vertexes;
    Order initOrder;
    
    inline void addEdge( size_t orig, size_t dest )  { vertexes[orig].push_front(dest); }
    size_t DFS(size_t v, Flags& notVisited, SCC& ssc) const;
    size_t magicDFS(size_t i, Flags& notVisited, size_t& t, Order& magicOrder ) const;
    Order getMagicOrder( const Graph& reversed ) const;
    void reorder( const Order& order );
    Graph( size_t size ) : vertexes(size) {}; 
public:
    Graph( const Graph & ) = delete;
    Graph& operator=( const Graph & ) = delete;
    Graph( Graph&& gr ) : vertexes(move(gr.vertexes)) { cerr << __PRETTY_FUNCTION__ << endl; };

    Graph( size_t size, const Edges& edges );
    
    list< SCC > getSCC() const;

    size_t size() const noexcept { return vertexes.size()-1; };
    string dump() const;
    size_t get( size_t i ) const { return initOrder[i]; };
};

Graph::Graph( size_t size, const Graph::Edges& edges )
: vertexes(++size), initOrder(size)
{
    #ifdef DEBUG
    cerr << "Building graph of size : "  << size << endl;
    #endif 
    iota(begin(initOrder), end(initOrder), 0);
    Graph reversed(size);
    for ( const Edge& edge : edges  )
    {
        if (edge.first >= size || edge.second >= size)
                            throw out_of_range("Bad edge");
        addEdge( edge.first, edge.second );
        reversed.addEdge( edge.second, edge.first );
        #ifdef DEBUG
        cerr << "Edge : "  << edge.first << " -> " << edge.second << endl;
        #endif 
    }
    
    Order magicOrder = reversed.getMagicOrder( reversed );
#ifdef DEBUG
//    cerr << "Graph built : " << endl << dump() << endl;
//    cerr << "Reversed    : " << endl << reversed.dump() << endl;
    cerr << "Magic order : ";
    for ( size_t i : magicOrder ) cerr << i << " ";
    cerr << endl;
#endif
    reorder(magicOrder);   
#ifdef DEBUG
    cerr << "Init order  : ";
    for ( size_t i : initOrder ) cerr << i << " ";
    cerr << endl;
#endif
}


list<Graph::SCC> Graph::getSCC() const 
{
    Flags notVisited(size()+1, true);
    list<SCC> res;
    size_t v = size();
    while (v > 0) {
        if (notVisited[v]) {
            SCC scc;
            DFS(v, notVisited, scc);
            res.push_back(move(scc));
        }
        v--;
    }    
    return res;
}

Graph::Order Graph::getMagicOrder( const Graph& reversed ) const
{
    Order magicOrder(size()+1, -1);
    Flags notVisited(size()+1, true);
    ssize_t i = size();
    size_t t = 0;
    while (i >= 0) {
        if (notVisited[i]) magicDFS(i, notVisited, t, magicOrder);
        i--;
    }
    return magicOrder;
}

size_t Graph::DFS(size_t i, Flags& notVisited, SCC& scc) const 
{
    size_t res = 1;
    notVisited[i] = false;
    scc.insert(get(i));
    for ( size_t j : vertexes[i] ) {
        if (!notVisited[j]) continue;
        res += DFS(j, notVisited, scc);
    }
    return res;
};

size_t Graph::magicDFS(size_t i, Flags& notVisited, size_t& t, Order& magicOrder) const 
{
    notVisited[i] = false;
    for ( size_t j : vertexes[i] )
        if (notVisited[j]) magicDFS(j, notVisited, t, magicOrder);
    magicOrder[i] = t++;
    return t;
};

void Graph::reorder( const Order& order )
{
    Vertexes newVertexes( vertexes.size() );
    Order backOrder(vertexes.size());
    for ( size_t i = 0; i < order.size(); i++ ) {
        for ( size_t& j : vertexes[i] ) j = order[j];
        swap( vertexes[i], newVertexes[order[i]] );
        backOrder[order[i]] = initOrder[i]; 
    }
    vertexes = move(newVertexes);
    swap(initOrder, backOrder);
}

string Graph::dump() const
{
    size_t i;
    ostringstream os;
    //matrix representation of a graph
    vector< vector< char > > matrix( size(), vector< char >(size(), ' '));

    /* Fill in the matrix */
    for( i = 1; i <= size(); i++ ) {
        matrix[i-1][i-1] = '+';
        for( const size_t dst : vertexes[i] )
            matrix[i-1][dst-1] = '*';
    }

    os.width(4); os << '+';
    for (i = 1; i <= size(); i++) {os.width(4); os << i;}
    os << endl;
    i = 1;
    for (vector< char > line : matrix) {
        os.width(4); os << i++;
        for (char ch : line) {os.width(4); os << ch;}
        os << endl;
    }
    return os.str();
}

int main(int argc, char* argv[])
{ 
    try {
        Graph::Edges edges;
        
        int size = 0;
        string istr;
        cin >> size;
        while (getline( cin, istr ))
        {
            istringstream iss (istr);
            int v,u;
            if (iss >> v && iss >> u)
            {
                edges.emplace_back(-v + size, u + size );
                edges.emplace_back(-u + size, v + size );

            } else {
                cerr << "Bad input string: " << istr;
            }
        }
        BENCHMARK_START
        Graph graph( 2*size, edges );
        BENCHMARK_FINISH( cerr, "Constructor")
        cerr << "Graph size : "<< graph.size() << endl;
#ifdef DEBUG
        //cerr <<  graph.dump() << endl;
#endif
        BENCHMARK_START
        list<Graph::SCC> SCCs = graph.getSCC(); 
        BENCHMARK_FINISH( cerr, "Connected components" )
        cerr << SCCs.size() << " strongly connected components found" << endl;
        
        bool isOk = true;
        int i = 0;
        for ( const Graph::SCC& scc : SCCs ) {
            i++;
            #ifdef DEBUG
                cerr << "SCC" << i << " : "  << scc.size() <<  endl;
            #endif            
            for ( ssize_t j : scc ) {
                if (j == size) continue;
                if (scc.find( -(j - size) + size ) != scc.end()) {
                    isOk = false;
                    cerr << "\tTrouble makers: " << (j - size) << " " <<  -(j - size) << endl;
                    break;
                }
            #ifdef DEBUG
                cerr << "\t" << int(j - size) << " - OK " <<  endl;
            #endif
            }
            if (!isOk) break;
        }
        cout << isOk << endl;
        return 0;
    } catch(exception& e) {
        cerr << "Error : " << e.what() << endl;
        return 1;
    }
}
