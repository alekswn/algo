/* 
 * Find strongly connected components of a directed graph 
 * Copyleft 2015  Alexey Novikov <alexey@novikov.io>
 */ 

#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <numeric>
#include <cassert>
#include <algorithm>
#include <iterator>
#include <cmath>
#include <list>
#include <forward_list>
#include <stack>
#include <queue>

using namespace std; //Very, very bad, but it's damn convenient for a small program


class Graph
{
public:
    typedef priority_queue< size_t, std::deque< size_t >, std::less<size_t> > PQ;
    typedef pair< size_t, size_t > Edge;
    typedef forward_list< Edge > Edges;
    typedef vector< size_t > Order;
    typedef vector< bool > Flags;
private:
    typedef vector< forward_list < size_t > > Vertexes;
    
    Vertexes vertexes;
    
    inline void addEdge( size_t orig, size_t dest )  { vertexes[orig].push_front(dest); }
    size_t DFS(size_t v, Flags& notVisited) const;
    size_t magicDFS(size_t i, Flags& notVisited, size_t& t, Order& magicOrder ) const;
    Order getMagicOrder( const Graph& reversed ) const;
    void reorder( const Order& order );
    Graph( size_t size ) : vertexes(size) {}; 
public:
    Graph( const Graph & ) = delete;
    Graph& operator=( const Graph & ) = delete;
    Graph( Graph&& gr ) : vertexes(move(gr.vertexes)) { cerr << __PRETTY_FUNCTION__ << endl; };

    Graph( size_t size, const Edges edges );
    
    PQ getSCC() const;

    size_t size() const noexcept { return vertexes.size()-1; };
    string dump() const;
};

Graph::Graph( size_t size, const Graph::Edges edges )
: vertexes(++size)
{
    Graph reversed(size);
    for ( const Edge& edge : edges  )
    {
        if (edge.first >= size || edge.second >= size)
                            throw out_of_range("Bad edge");
        addEdge( edge.first, edge.second );
        reversed.addEdge( edge.second, edge.first );
    }
    
    Order magicOrder = reversed.getMagicOrder( reversed );
#ifdef DEBUG
    cerr << "Graph built : " << endl << dump() << endl;
    cerr << "Reversed    : " << endl << reversed.dump() << endl;
    cerr << "Magic order : ";
    for ( size_t i : magicOrder ) cerr << i << " ";
    cerr << endl;
#endif
    reorder(magicOrder);   
}

int main(int argc, char* argv[])
{ 
    try {
        Graph::Edges edges;
        
        size_t size = 0;
        string istr;
        while (getline( cin, istr ))
        {
            istringstream iss (istr);
            uint v,u;
            if (iss >> v && iss >> u)
            {
               if (v > size) size = v;
               if (u > size) size = u;
               edges.emplace_front( Graph::Edge(v,u) );
            } else {
                cerr << "Bad input string: " << istr;
            }
        }
        Graph graph( size, edges );
        cerr << "Graph size : "<< graph.size() << endl;
#ifdef DEBUG
        cerr <<  graph.dump() << endl;
#endif
        Graph::PQ SCCs = graph.getSCC(); 
        int c = 5;
        while (!SCCs.empty() && c-- > 0) {
            cout << SCCs.top() << ",";
            SCCs.pop();
        }
        cout << endl;
         
        return 0;
    } catch(exception& e) {
        cerr << "Error : " << e.what() << endl;
        return 1;
    }
}

Graph::PQ Graph::getSCC() const 
{
    Flags notVisited(size()+1, true);
    PQ pq;
    size_t v = size();
    while (v > 0) {
        if (notVisited[v]) pq.push(DFS(v, notVisited));
        v--;
    }    
    return pq;
}

Graph::Order Graph::getMagicOrder( const Graph& reversed ) const
{
    Order magicOrder(size()+1, 0);
    Flags notVisited(size()+1, true);
    size_t i = size();
    size_t t = 0;
    while (i > 0) {
        if (notVisited[i]) magicDFS(i, notVisited, t, magicOrder);
        i--;
    }
    return magicOrder;   
}

size_t Graph::DFS(size_t i, Flags& notVisited) const 
{
    size_t sz = 1;
    notVisited[i] = false;

    for ( size_t j : vertexes[i] ) {
        if (notVisited[j]) sz+=DFS(j, notVisited);
    }
    return sz;
};

size_t Graph::magicDFS(size_t i, Flags& notVisited, size_t& t, Order& magicOrder) const 
{
    notVisited[i] = false;
    for ( size_t j : vertexes[i] )
        if (notVisited[j]) magicDFS(j, notVisited, t, magicOrder);
    magicOrder[i] = ++t;
    return t;
};

void Graph::reorder( const Order& order )
{
    Vertexes newVertexes( vertexes.size() );
    for ( size_t i = 0; i < order.size(); i++ ) {
        for ( size_t& j : vertexes[i] ) j = order[j];
        swap( vertexes[i], newVertexes[order[i]] );
    }
    vertexes = move(newVertexes);
}

string Graph::dump() const
{
    size_t i;
    ostringstream os;
    //matrix representation of a graph
    vector< vector< char > > matrix( size(), vector< char >(size(), ' '));

    /* Fill in the matrix */
    for( i = 1; i <= size(); i++ ) {
        matrix[i-1][i-1] = '+';
        for( const size_t dst : vertexes[i] )
            matrix[i-1][dst-1] = '*';
    }

    os.width(4); os << '+';
    for (i = 1; i <= size(); i++) {os.width(4); os << i;}
    os << endl;
    i = 1;
    for (vector< char > line : matrix) {
        os.width(4); os << i++;
        for (char ch : line) {os.width(4); os << ch;}
        os << endl;
    }
    return os.str();
}
