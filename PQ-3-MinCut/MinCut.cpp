/* 
 * Find minimum cut on a unordered graph 
 * Copyleft 2015  Alexey Novikov <alexey@novikov.io>
 */ 

#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <numeric>
#include <random>
#include <cassert>
#include <algorithm>
#include <iterator>
#include <set>
#include <cmath>

using namespace std; //Very, very bad, but it's damn convenient for a small program

typedef uint_fast8_t index_t;
const double DEF_MAX_P_FAIL_ALL = 0.01;

class Graph
{
    //Algorithm adopted from the book "Algorithms, 4th Edition" by Sedgewick and Wayne
    class WeightedQuickUnionUF 
    {
        size_t      _count; // number of components
        vector<index_t> id; // parent link (site indexed)
        vector<size_t>  sz; // size of component for roots (site indexed)
    public:
        WeightedQuickUnionUF(size_t N)
        : _count(N), id(N), sz(N,1)
        {
#ifdef TRACE
            cerr << __PRETTY_FUNCTION__ << " ( " << (int)N << " ) " << endl; 
#endif
            iota(id.begin(), id.end(), 0);
        }
    private:
        index_t find(index_t p) const
        { // Follow links to find a root.
#ifdef TRACE
            cerr << __PRETTY_FUNCTION__ << " ( " << (int)p << " ) " << endl; 
#endif
            while (p != id[p]) p = id[p];
            return p;
        }
    public:
        size_t count() const noexcept { return _count; }
        bool   connected(index_t p, index_t q) const 
        {
#ifdef TRACE
            cerr << __PRETTY_FUNCTION__ << " ( " << (int)p << " , " 
                    << (int)q << " ) " << endl; 
#endif
            return find(p) == find(q); 
        }
        void   contract(index_t p, index_t q)
        {
#ifdef TRACE
            cerr << __PRETTY_FUNCTION__ << " ( " << (int)p << " , " 
                    << (int)q << " ) " << endl; 
#endif
            index_t i = find(p);
            index_t j = find(q);
            assert( i < id.size() && j < id.size() );
            if (i == j) return;
            // Make smaller root point to larger one.
            if (sz[i] < sz[j]) 
            { 
                id[i] = j; 
                sz[j] += sz[i]; 
            } else { 
                id[j] = i; 
                sz[i] += sz[j]; 
            }
            _count--;
        }
    };
public:
    typedef pair< index_t, index_t> Edge;
    typedef vector< Edge > Edges;
    typedef set< Edge >::const_iterator inputIterator; 
private:
    size_t sz;
    Edges edges;

public:
    Graph() noexcept : sz(0) {};
    Graph( size_t size, inputIterator first, inputIterator last );

    size_t size() const noexcept { return sz; };
    bool good() const;
    string dump() const;

    Edges minCut(double) const;
protected:
    Edges randomCut() const;
};

Graph::Graph( size_t size, Graph::inputIterator first, Graph::inputIterator last )
: sz(size), edges( first, last )
{
    if (!good()) throw runtime_error("Bad graph constructed");
}

Graph::Edges Graph::randomCut() const 
{
    WeightedQuickUnionUF UF(size());      //Union-Find array for vertexes.
    vector< size_t > seq(edges.size(), 0);  //Shuffeled array of indexes for selecting
    iota(seq.begin(), seq.end(), 0);  //edges in random order.
    shuffle(seq.begin(), seq.end(), mt19937{random_device{}()});
    vector< size_t >::iterator it;   //Index in "seq[]"
    
    for (it = seq.begin(); UF.count() > 2; it++) //reduce the number of independed vertexes
    {
        assert( it != seq.end() );
        UF.contract(edges[*it].first, edges[*it].second);
    }
        
    Edges res;
    for_each(it, seq.end(), [&UF, &res, this](size_t i) mutable -> void
    {
        if (!UF.connected( edges[i].first, edges[i].second)) res.push_back(edges[i]);
    } ); 
    return res;
};

bool Graph::good() const
{
    WeightedQuickUnionUF UF(size());      //Union-Find array for vertexes.
    for ( Edge edge : edges ) {
        if ( edge.first < size() || edge.second < size() ) {//Bounds check
            UF.contract( edge.first, edge.second );
        } else {
#ifdef DEBUG
            cerr << __PRETTY_FUNCTION__ <<": Inconsistent set of edges given" << endl;
#endif
            return false;
        }
    }
    //Check if graph is connected
    if ( UF.count() != 1 ) {
#ifdef DEBUG
        cerr << __PRETTY_FUNCTION__ <<": Graph is not connected" << endl;
#endif
        return false;
    }

    return true;
}

Graph readGraph( istream& s ) 
{
    string istr;
    set< Graph::Edge > list;
    size_t size = 0;
    while (getline( cin, istr ))
    {
        istringstream iss (istr);
        uint v,u;
        if (iss >> v) { size++; v--; }
        while (iss >> u) {
            u--;
            if (list.find(Graph::Edge(u,v)) == list.end())
                list.emplace( Graph::Edge(v,u) );
            }
    }
    Graph graph( size, list.begin(), list.end() );
#ifdef DEBUG
    cerr <<  graph.dump() << endl;
#endif
    return graph; 
}

int main(int argc, char* argv[])
{
    double maxPfailAll = (argc > 1) ? strtod(argv[1],NULL) : DEF_MAX_P_FAIL_ALL;
    try {
        Graph graph = readGraph(cin);
        cerr << "Graph size : "<< graph.size() << endl;
        cout << graph.minCut(maxPfailAll).size() << endl;
        return 0;
    } catch(exception& e) {
        cerr << "Error : " << e.what() << endl;
        return 1;
    }
}

Graph::Edges Graph::minCut(double maxPfailAll) const
{
    Edges minCut = randomCut();
    size_t N = size();
    double Pfail1 = ( 1.0 - 1.0/((double)N*(double)N));
    double PfailAll = Pfail1;
#ifdef DEBUG
    cerr << "Pr[all trials fail] <= " << PfailAll;
    uint i = 1;    
#endif
    while (PfailAll >= maxPfailAll) 
    {
        Edges cut = randomCut();
        PfailAll *= Pfail1;
        if (cut.size() < minCut.size())
        {
            minCut = move(cut);
#ifdef DEBUG
            cerr << endl <<"New minimum cut of size " << minCut.size() 
                 << " found on iteration " << i << " : ";
            for (Edge edge : minCut) 
                cerr << " (" << int(edge.first+1) << ", " << int(edge.second+1) << ")";
            cerr << endl;
#endif
        }
#ifdef DEBUG
        i++;
        if (i%N == 0) cerr << "\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b"
                           << "\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b"
                        << i
                        << " Pr[all trials fail] <= "  << PfailAll;
#endif
    }
    
    return minCut; 
}

string Graph::dump() const
{
    uint i;
    ostringstream os;
    //matrix representation of a graph
    vector< vector< char > > matrix( size(), vector< char >(size(), ' '));

    for( i = 0; i < size(); i++ ) matrix[i][i] = '+';
    for( Edge edge : edges )
        matrix[edge.first][edge.second] = '*'; 

    os.width(4); os << '+';
    for (i = 1; i <= size(); i++) {os.width(4); os << i;}
    os << endl;
    i = 1;
    for (vector< char > line : matrix) {
        os.width(4); os << i++;
        for (char ch : line) {os.width(4); os << ch;}
        os << endl;
    }
    return os.str();
}
